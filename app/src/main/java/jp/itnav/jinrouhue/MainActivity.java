package jp.itnav.jinrouhue;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;

import com.philips.lighting.model.PHLight;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends BaseHueActivity implements View.OnClickListener {
    private static final int LIGHT_TYPE_MEETING = 0;
    private static final int LIGHT_TYPE_VOTE = 1;
    private static final int LIGHT_TYPE_NIGHT = 2;
    private static final int LIGHT_TYPE_WIN_HUMAN = 3;
    private static final int LIGHT_TYPE_WIN_WOLFMAN = 4;

    private Timer mLightTimer;
    private int mCurrentLightIndex = 0;
    private int mCurrentLightMode = LIGHT_TYPE_MEETING;

    private SeekBar brightnessBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setLayout();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.light_meeting:
                mCurrentLightMode = LIGHT_TYPE_MEETING;
                break;
            case R.id.light_night:
                mCurrentLightMode = LIGHT_TYPE_NIGHT;
                break;
            case R.id.light_vote:
                mCurrentLightMode =LIGHT_TYPE_VOTE;
                break;
            case R.id.light_win_human:
                mCurrentLightMode =LIGHT_TYPE_WIN_HUMAN;
                break;
            case R.id.light_win_wolfman:
                mCurrentLightMode =LIGHT_TYPE_WIN_WOLFMAN;
                break;
        }

//        if (mStreamId != 0) {
//            mSoundPool.stop(mStreamId);
//        }
//        mStreamId = mSoundPool.play(mSoundIds[mCurrentColorPattern], 1.0F, 1.0F, 0, 0, 1.0F);
//        previewCurrentLightColor();
//        changeActiveColorButtonImage();
        updateLights();
    }

    private EditText red;
    private EditText green;
    private EditText blue;

    private void setLayout() {
        ImageButton meeting = (ImageButton) findViewById(R.id.light_meeting);
        ImageButton night = (ImageButton) findViewById(R.id.light_night);
        ImageButton vote = (ImageButton) findViewById(R.id.light_vote);
        ImageButton winHuman = (ImageButton) findViewById(R.id.light_win_human);
        ImageButton winWolfman = (ImageButton) findViewById(R.id.light_win_wolfman);
        brightnessBar = (SeekBar) findViewById(R.id.brightness_bar);

        red = (EditText) findViewById(R.id.form_red);
        green = (EditText) findViewById(R.id.form_green);
        blue = (EditText) findViewById(R.id.form_blue);

        meeting.setOnClickListener(this);
        night.setOnClickListener(this);
        vote.setOnClickListener(this);
        winHuman.setOnClickListener(this);
        winWolfman.setOnClickListener(this);

        Button lightChange = (Button) findViewById(R.id.button_change);
        lightChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeLights();
            }
        });

        brightnessBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
//                updateLights();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                updateLights();
            }
        });
    }


    public void initializeLightTimer() {
        if (mLightTimer != null) {
            mLightTimer.cancel();
        }
        mLightTimer = new Timer(true);
    }

    public void updateLights() {
        if (!isBridgeConnected()) {
            return;
        }
        initializeLightTimer();

        Log.d("mainActivity", "id: " + getLightIdList());
        mLightTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                lightHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        List<PHLight> allLights = getReachableLights();
                        PHLight light = allLights.get(mCurrentLightIndex);
                        updateLightState(light, getLightRGBColor(light, getColorRed(mCurrentLightMode),
                                getColorGreen(mCurrentLightMode), getColorBlue(mCurrentLightMode)), brightnessBar.getProgress());
                        checkContinueAction(allLights.size(), true);
                    }
                });
            }
        }, Constants.TIMER_PERIOD, Constants.TIMER_DELAY);
    }

    public void changeLights() {
        if (!isBridgeConnected()) {
            return;
        }
        initializeLightTimer();
        mLightTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                lightHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        List<PHLight> allLights = getReachableLights();
                        PHLight light = allLights.get(mCurrentLightIndex);
                        updateLightState(light, getLightRGBColor(light,
                                Integer.parseInt(red.getText().toString()),
                                Integer.parseInt(green.getText().toString()),
                                Integer.parseInt(blue.getText().toString())),
                                brightnessBar.getProgress());
                        checkContinueAction(allLights.size(), true);
                    }
                });
            }
        }, Constants.TIMER_PERIOD, Constants.TIMER_DELAY);
    }


    public synchronized void checkContinueAction(int lightSize, boolean isDarkenLights) {
        if (mCurrentLightIndex + 1 < lightSize) {
            mCurrentLightIndex++;
        }
        else {
            mLightTimer.cancel();
            if (isDarkenLights) {
//                darkenLights();
            }
            mCurrentLightIndex = 0;
        }
    }

    public void darkenLights() {
        initializeLightTimer();
        mLightTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                lightHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        List<PHLight> allLights = getSortedLights();
                        PHLight light = allLights.get(mCurrentLightIndex);
                        updateLightBrightness(light, Constants.LIGHT_MIN_BRIGHTNESS);
                        checkContinueAction(allLights.size(), false);
                    }
                });
            }
        }, Constants.TIMER_DELAY_DARKEN, Constants.TIMER_PERIOD);
    }



    public int getColorRed(int lightMode) {
        return Constants.COLOR_PATTERNS[lightMode][Constants.COLOR_PATTERN_RED];
    }

    public int getColorGreen(int lightMode) {
        return Constants.COLOR_PATTERNS[lightMode][Constants.COLOR_PATTERN_GREEN];
    }

    public int getColorBlue(int lightMode) {
        return Constants.COLOR_PATTERNS[lightMode][Constants.COLOR_PATTERN_BLUE];
    }
}
