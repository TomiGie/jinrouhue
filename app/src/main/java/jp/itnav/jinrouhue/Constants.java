package jp.itnav.jinrouhue;

import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;

public class Constants {
    public static final int[][] COLOR_PATTERNS = {
            {255, 251, 15},  // talk 黄色、緑
            {204, 94, 6}, // vote オレンジ、紫の間
            {49, 0, 138},  // night
            {255, 238, 5},   // win human
            {255, 0, 0}   // win wolfman
    };

    public static final int[] SOUND_RESOURCES = {
//            R.raw.syou_oto,
//            R.raw.syou_oto,
//            R.raw.syou_oto,
//            R.raw.syou_oto,
//            R.raw.syou_oto,
//            R.raw.dai_oto,
    };

    public static final int COLOR_PATTERN_RED = 0;
    public static final int COLOR_PATTERN_GREEN = 1;
    public static final int COLOR_PATTERN_BLUE = 2;
    public static final int LIGHT_MAX_BRIGHTNESS = 254;
    public static final int LIGHT_MIN_BRIGHTNESS = 0;

    public static final int TIMER_PERIOD = 150;
    public static final int TIMER_DELAY = 5;
    public static final int TIMER_DELAY_DARKEN = 5000;

    // for Play Sound
    public static SoundPool buildSoundPool() {
        SoundPool pool;
        int poolMax = SOUND_RESOURCES.length;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            pool = new SoundPool(poolMax, AudioManager.STREAM_MUSIC, 0);
        }
        else {
            AudioAttributes attr = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();

            pool = new SoundPool.Builder()
                    .setAudioAttributes(attr)
                    .setMaxStreams(poolMax)
                    .build();
        }
        return pool;
    }
}
