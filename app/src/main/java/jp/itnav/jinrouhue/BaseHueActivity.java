package jp.itnav.jinrouhue;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;

import com.philips.lighting.hue.listener.PHLightListener;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.hue.sdk.utilities.PHUtilities;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHBridgeResource;
import com.philips.lighting.model.PHHueError;
import com.philips.lighting.model.PHLight;
import com.philips.lighting.model.PHLightState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public abstract class BaseHueActivity extends AppCompatActivity implements PHLightListener {

    protected PHHueSDK  phHueSDK;
    protected Handler lightHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        phHueSDK = PHHueSDK.create();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        lightHandler = new Handler();
    }

    @Override
    protected void onDestroy() {
        PHBridge bridge = getSelectedBridge();
        if (bridge != null) {
            if (phHueSDK.isHeartbeatEnabled(bridge)) {
                phHueSDK.disableHeartbeat(bridge);
            }
            phHueSDK.disconnect(bridge);
        }
        super.onDestroy();
    }

    /**
     * implements PHLightListener
     * @param phLight
     */
    @Override
    public void onReceivingLightDetails(PHLight phLight) {
        Log.d("BaseHue", "onReceivingLightDetails");
    }

    @Override
    public void onReceivingLights(List<PHBridgeResource> list) {
        Log.d("BaseHue", "onReceivingLights");
    }

    @Override
    public void onSearchComplete() {
    }

    @Override
    public void onSuccess() {
        Log.d("BaseHue", "onSuccess");
    }

    @Override
    public void onError(int i, String s) {

        Log.d("BaseHue", "onError i: " + i + ", msg: " + s);
    }

    @Override
    public void onStateUpdate(Map<String, String> map, List<PHHueError> list) {
        Log.d("BaseHue", "onStateUpdate");
    }

    protected boolean isBridgeConnected() {
        // Bridgeがゲットできればtrueを返す
        return getSelectedBridge() != null;
    }

    protected PHBridge getSelectedBridge() {
        return phHueSDK.getSelectedBridge();
    }

    protected ArrayList<Integer> getLightIdList() {
        List<PHLight> lightList = getSortedLights();
        int lightIdList[] = new int[lightList.size()];
        ArrayList<Integer> reachableLightList = new ArrayList<>();
        for (int i = 0; i < lightIdList.length; i++) {
            PHLight light = lightList.get(i);
            if (light.getLastKnownLightState().isReachable()) {
                lightIdList[i] = getLightId(light);
                reachableLightList.add(lightIdList[i]);
            }
        }
        return reachableLightList;
    }

    protected int getLightId(PHLight light) {
        return Integer.parseInt(light.getIdentifier());
    }

    protected float[] getLightRGBColor(PHLight light, int red, int green, int blue) {
        return PHUtilities.calculateXYFromRGB(red, green, blue, light.getModelNumber());
    }

    public synchronized void updateLightBrightness(PHLight light, int brightness) {
        PHBridge bridge = getSelectedBridge();
        PHLightState lightState = new PHLightState();
        lightState.setBrightness(brightness);
        bridge.updateLightState(light, lightState, this);
    }

    protected synchronized void updateLightState(PHLight light, float[] rgb, int brightness) {
        PHBridge bridge = getSelectedBridge();
        PHLightState lightState = new PHLightState();
        lightState.setX(rgb[0]);
        lightState.setY(rgb[1]);
        lightState.setBrightness(brightness);
        bridge.updateLightState(light, lightState, this);
    }

    protected List<PHLight> getSortedLights() {
        PHBridge bridge = getSelectedBridge();
        List<PHLight> allLights = bridge.getResourceCache().getAllLights();
        Collections.sort(allLights, new LightComparator());
        return allLights;
    }

    protected ArrayList<PHLight> getReachableLights() {
        PHBridge bridge = getSelectedBridge();
        List<PHLight> allLights = bridge.getResourceCache().getAllLights();
        Collections.sort(allLights, new LightComparator());

        ArrayList<PHLight> reachableLights = new ArrayList<>();
        for (int i = 0; i < allLights.size(); i++) {
            PHLight light = allLights.get(i);
            if (light.getLastKnownLightState().isReachable()) {
                reachableLights.add(light);
            }
        }
        return reachableLights;
    }

    protected class LightComparator implements Comparator<PHLight> {
        @Override
        public int compare(PHLight l1, PHLight l2) {
            return Integer.parseInt(l1.getIdentifier()) < Integer.parseInt(l2.getIdentifier()) ? -1 : 1;
        }
    }
}
